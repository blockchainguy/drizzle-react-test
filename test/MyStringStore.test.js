const MyStringStore = artifacts.require('MyStringStore.sol');

contract('MyStringStore', ([account]) => {
  it('should store the string', async () => {
    const store = await MyStringStore.deployed();
    await store.set('Hey there!', { from: account });
    const str = await store.myString.call();

    assert.equal(str, 'Hey there!', 'String not properly set!');
  });
});
