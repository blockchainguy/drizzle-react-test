require('babel-register');
require('babel-polyfill');

require('dotenv').config();

const HDWalletProvider = require('truffle-hdwallet-provider');

const ApiKey = process.env.INFURA_API_KEY;

module.exports = {
  networks: {
    development: {
      host: 'localhost',
      port: 8545,
      network_id: '*', // eslint-disable-line camelcase
    },
    ropsten: {
      provider: new HDWalletProvider(process.env.MNEMONIC, `https://ropsten.infura.io/v3/${ApiKey}`),
      network_id: 3, // eslint-disable-line camelcase
      gas: 3712388,
      // gasPrice: 2700000000000,
    },
    kovan: {
      provider: new HDWalletProvider(process.env.MNEMONIC, `https://kovan.infura.io/v3/${ApiKey}`),
      network_id: 42, // eslint-disable-line camelcase
      gas: 3712388,
      // gasPrice: 2700000000000,
    }
  },
};
